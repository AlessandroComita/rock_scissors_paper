
// set the number of winning moves needed to win the match
var goal = 10;


// inizializza conteggio partite
let playerWonMatches = 0;
let cpuWonMatches = 0;

// inizializza score
let userScore = 0;
let cpuScore = 0;


// grab music element
var e_music = document.getElementById('music');
var e_egg = document.getElementById('egg');
var e_sparkle = document.getElementById('sparkle');
var e_buzz = document.getElementById('buzz');
var e_down = document.getElementById('down');
var e_lost = document.getElementById('lost');
var e_win = document.getElementById('win');
var e_bubble = document.getElementById('bubble');



// set a variable to control music
var musicPlaying = false;

// set a variable to toggle sound fx
var soundFX = true;


// cattura finestra principale gioco
var e_gameMainWindow = document.getElementById('gameMainWindow');

// cattura landing page
var e_landing = document.getElementById('landing');

// cattura tasto play
var e_bottonePlay = document.getElementById('bottonePlay');



// cattura schermata risultati
var e_resultsPage = document.getElementById('resultsPage');


// cattura tasto play again
var e_bottonePlayAgain = document.getElementById('bottonePlayAgain');


// cattura elementi punteggio finale
var e_playerFinalScore = document.getElementById('playerFinalScore');
var e_cpuFinalScore = document.getElementById('cpuFinalScore');

// cattura contatore partite vinte
var e_playerWins = document.getElementById('playerWins');
var e_cpuWins = document.getElementById('cpuWins');

// cattura paragrafo messaggio finale
var e_finalMessage = document.getElementById('finalMessage');



// cattura controlli audio
var e_audioControls = document.getElementById('audioControls');
var e_iconaMusica = document.getElementById('iconaMusica');
var e_iconaSpeaker = document.getElementById('iconaSpeaker');







// comportamento alla pressione del pulsante play
e_bottonePlay.addEventListener('click',()=>
{
    e_landing.classList.remove('top-0');
    e_landing.style.bottom = '-1600px';
    e_gameMainWindow.classList.remove('d-none');
    e_audioControls.classList.remove('d-none');
    if (soundFX) {e_egg.play();};
    
    e_music.play(); 
    musicPlaying = true;
});

// comportamento tasto toggle musica
e_iconaMusica.addEventListener('click', ()=>
{
    if (musicPlaying)
    {
        e_egg.pause();
        e_egg.currentTime = 0;
        if (soundFX) {e_egg.play();};
        
        e_iconaMusica.style.opacity = "0.3";
        
        e_music.pause();
        e_music.currentTime = 0;
        musicPlaying = false;
    }
    else
    {
        e_egg.pause();
        e_egg.currentTime = 0;
        if (soundFX) {e_egg.play();};
        
        e_iconaMusica.style.opacity = "1";

        e_music.play();
        e_music.currentTime = 0;
        musicPlaying = true;
    }
    
})

// comportamento tasto toggle sound fx
e_iconaSpeaker.addEventListener("click", ()=>
{
    if (soundFX)
    {
        e_egg.pause();
        e_egg.currentTime = 0;
        e_egg.play();

        e_iconaSpeaker.style.opacity = "0.3";

        soundFX = false;
    }
    else
    {
        e_egg.pause();
        e_egg.currentTime = 0;
        e_egg.play();

        e_iconaSpeaker.style.opacity = "1";

        soundFX = true;       

    }
})


// comportamento pressione tasto play again
e_bottonePlayAgain.addEventListener('click', ()=>
{

    if (soundFX) {e_egg.play();};


    // fa sparire box risultati
    e_resultsPage.style.top = '-800px';

    // aggiorna messaggio per nuovo match
    e_messaggio.innerHTML = 'Good Luck!'

    // resetta punteggi
    userScore=0;
    cpuScore=0;

    e_playerScore.innerHTML = userScore;
    e_cpuScore.innerHTML = cpuScore;
    
    // riporta la finestra principale a piena opacità
    e_gameMainWindow.style.opacity = '1';
    
    
    
});



var e_messaggio = document.getElementById('message');



var cpuChoice = '';


// elementi giocata
var e_playerGame = document.getElementById('playerGame');
var e_cpuGame = document.getElementById('cpuGame');



// ELEMENTI SCORE

// cattura
var e_playerScore = document.getElementById('playerScore');
var e_cpuScore = document.getElementById('cpuScore');

// inserisci risultato
e_playerScore.innerHTML = userScore;
e_cpuScore.innerHTML = userScore;




// cattura bottoni azione
var e_rockButton = document.getElementById('rockButton');
var e_scissorsButton = document.getElementById('scissorsButton');
var e_paperButton = document.getElementById('paperButton');



// funzione calcola giocata CPU
function cpuHand()
{
    cpuChoice = Math.floor(Math.random()*3) + 1;
    console.log(cpuChoice);
    
    switch (cpuChoice)
    {
        case 1:
        console.log('CPU plays ROCK');
        cpuChoice='ROCK';
        e_cpuGame.setAttribute('src', 'resources/images/rock.png');
        
        e_cpuGame.classList.add('appearAnimation');
        setTimeout(function()
        {
            e_cpuGame.classList.remove('appearAnimation');
        },400)
        
        
        break;
        case 2:
        console.log('CPU plays SCISSORS');
        cpuChoice='SCISSORS';
        e_cpuGame.setAttribute('src', 'resources/images/scissors.png');
        
        
        e_cpuGame.classList.add('appearAnimation');
        setTimeout(function()
        {
            e_cpuGame.classList.remove('appearAnimation');
        },400)
        
        break;
        case 3:
        console.log('CPU plays PAPER');
        cpuChoice='PAPER';
        e_cpuGame.setAttribute('src', 'resources/images/paper.png');
        
        e_cpuGame.classList.add('appearAnimation');
        setTimeout(function()
        {
            e_cpuGame.classList.remove('appearAnimation');
        },400)
        
        
        break;
        default:
        console.log('CPU does not play');
        break;   
    }
}



// funzione calcola chi vince il match
function match()
{
        
    if (userScore==goal)
    {
        playerWonMatches++;
    
        e_playerScore.innerHTML = userScore;
        e_cpuScore.innerHTML = cpuScore;

        e_playerFinalScore.innerHTML = userScore;
        e_cpuFinalScore.innerHTML = cpuScore;
  
        e_finalMessage.innerHTML = `Congratulations!<br> You won the match!`;

        e_playerWins.innerHTML = playerWonMatches;
        e_cpuWins.innerHTML = cpuWonMatches;

        if (soundFX) {e_win.play();};

        // e_resultsPage.classList.remove('d-none');
        e_resultsPage.style.top = '25px';
        e_gameMainWindow.style.opacity = '0.3';
        
    }
    
    if (cpuScore==goal)
    {

        cpuWonMatches++;

        e_playerScore.innerHTML = userScore;
        e_cpuScore.innerHTML = cpuScore;

        e_playerFinalScore.innerHTML = userScore;
        e_cpuFinalScore.innerHTML = cpuScore;

        e_finalMessage.innerHTML = `Too bad!<br> You lost the match!`;

        e_playerWins.innerHTML = playerWonMatches;
        e_cpuWins.innerHTML = cpuWonMatches;
    
        e_playerScore.innerHTML = userScore;
        e_cpuScore.innerHTML = cpuScore;

        if (soundFX) {e_lost.play();};

        e_resultsPage.style.top = '25px';
        e_gameMainWindow.style.opacity = '0.3';
        
    }
}



// funzione vince, aumenta risultato e mostra vincita
function vinceGiocatore()
{
    e_playerScore.classList.add('textAnimation');
    setTimeout(function()
    {
        e_playerScore.classList.remove('textAnimation');
    },400)
    e_messaggio.innerHTML = 'You scored!';
    userScore++;
    e_playerScore.innerHTML = userScore;
    e_sparkle.pause();
    e_sparkle.currentTime = 0;
    if (soundFX) {e_sparkle.play();};
    
}

function vinceCPU()
{
    e_cpuScore.classList.add('textAnimation');
    setTimeout(function()
    {
        e_cpuScore.classList.remove('textAnimation');
    },400)
    
    
    e_messaggio.innerHTML = 'CPU scored!';
    cpuScore++;
    e_cpuScore.innerHTML = cpuScore;
    e_down.pause();
    e_down.currentTime = 0;
    if (soundFX) {e_down.play();};

}

function pari()
{
    console.log('TIE!')
    e_messaggio.innerHTML = 'Tie!';
    
    e_bubble.pause();
    e_bubble.currentTime = 0;
    if (soundFX) {e_bubble.play();};
}

// funzione calcola chi vince la mano 

function vincente ()
{
    if (userChoice == cpuChoice)
    {
        pari();
    }
    else if (userChoice == 'ROCK' && cpuChoice == 'SCISSORS')
    {
        vinceGiocatore()
    }
    else if (userChoice == 'ROCK' && cpuChoice == 'PAPER')
    {
        vinceCPU();
        
    }
    else if (userChoice == 'PAPER' && cpuChoice == 'ROCK')
    {
        vinceGiocatore()
    }
    else if (userChoice == 'PAPER' && cpuChoice == 'SCISSORS')
    {
        vinceCPU();
    }
    else if (userChoice == 'SCISSORS' && cpuChoice == 'PAPER')
    {
        vinceGiocatore()
    }
    else if (userChoice == 'SCISSORS' && cpuChoice == 'ROCK')
    {
        vinceCPU();
    }
    
    
    
}


e_rockButton.addEventListener("click",()=>
{ 
    e_playerGame.setAttribute('src', 'resources/images/rock.png');
    
    
    e_playerGame.classList.add('appearAnimation');
    setTimeout(function()
    {
        e_playerGame.classList.remove('appearAnimation');
    },400)
    
    
    cpuHand();
    userChoice='ROCK'; 
    
    vincente();
    match();
})

e_scissorsButton.addEventListener("click",()=>
{   
    e_playerGame.setAttribute('src', 'resources/images/scissors.png');
    
    e_playerGame.classList.add('appearAnimation');
    setTimeout(function()
    {
        e_playerGame.classList.remove('appearAnimation');
    },400)
    
    
    cpuHand();
    userChoice='SCISSORS';
    vincente();
    match();
})

e_paperButton.addEventListener("click",()=>
{   
    e_playerGame.setAttribute('src', 'resources/images/paper.png');
    
    e_playerGame.classList.add('appearAnimation');
    setTimeout(function()
    {
        e_playerGame.classList.remove('appearAnimation');
    },400)
    
    
    cpuHand();
    userChoice='PAPER';
    vincente();
    match();
})

